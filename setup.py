'''
                                  ESP Health
                                 Anaplasmosis
                             Packaging Information


@author: Jeff Andre <jandre@commoninf.com>
@organization: Commonwealth Informatics https://commoninf.com
@contact: http://www.esphealth.org
@copyright: (c) 2019
@license: LGPL 3.0 http://www.gnu.org/licenses/lgpl-3.0.txt
'''

from setuptools import find_packages
from setuptools import setup

setup(
    name='esp-plugin_anaplasmosis',
    version='1.2',
    author='Jeff Andre',
    author_email='jandre@commoninf.com',
    description='Anaplasmosis disease definition module for ESP Health application',
    license='LGPLv3',
    keywords='Anaplasmosis algorithm disease surveillance public health epidemiology',
    url='http://esphealth.org',
    packages=find_packages(exclude=['ez_setup']),
    install_requires=[
    ],
    entry_points='''
        [esphealth]
        disease_definitions = anaplasmosis:disease_definitions
        event_heuristics = anaplasmosis:event_heuristics
    '''
)
