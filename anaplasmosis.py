'''
                                 ESP Health
                        Notifiable Diseases Framework
                         Anaplasmosis Case Generator


@author: Jeff Andre <jandre@commoninf.com>
@organization: commonwealth informatics https://www.commoninf.com
@contact: https://www.esphealth.org
@copyright: (c) 2019 Commonwealth Informatics, Inc.
@license: LGPL
'''

# In most instances it is preferable to use relativedelta for date math.  
# However when date math must be included inside an ORM query, and thus will
# be converted into SQL, only timedelta is supported.
from dateutil.relativedelta import relativedelta
import datetime

from django.db.models import F

from ESP.utils import log
from ESP.hef.base import Event
from ESP.hef.base import LabResultPositiveHeuristic, PrescriptionHeuristic, DiagnosisHeuristic
from ESP.hef.base import Dx_CodeQuery
from ESP.nodis.base import DiseaseDefinition
from ESP.nodis.models import Case
from ESP.static.models import DrugSynonym
from ESP.conf.models import LabTestMap

class Anaplasmosis(DiseaseDefinition):
    '''
    Anaplasmosis
    '''
    
    conditions = ['anaplasmosis']
    
    uri = 'urn:x-esphealth:disease:channing:anaplasmosis:v1'
    
    short_name = 'anaplasmosis'
    
    test_name_search_strings = ['phago','hge','anaplasma','ehrlic']

    timespan_heuristics = []

    recurrence = 365

    @property
    def event_heuristics(self):
        heuristic_list = []
        #
        # Diagnosis Codes
        #
        heuristic_list.append( DiagnosisHeuristic(
            name = 'anaplasmosis',
            dx_code_queries = [
                Dx_CodeQuery(starts_with='082.49', type='icd9'),
                Dx_CodeQuery(starts_with='A77.49', type='icd10'),
                ]
            ))
        heuristic_list.append( DiagnosisHeuristic(
            name = 'fever_unspecified',
            dx_code_queries = [
                Dx_CodeQuery(starts_with='780.60', type='icd9'),
                Dx_CodeQuery(starts_with='R50.9', type='icd10'),
                ]
            ))
        heuristic_list.append( DiagnosisHeuristic(
            name = 'chills',
            dx_code_queries = [
                Dx_CodeQuery(starts_with='780.64', type='icd9'),
                Dx_CodeQuery(starts_with='R68.83', type='icd10'),
                ]
            ))
        heuristic_list.append( DiagnosisHeuristic(
            name = 'headache',
            dx_code_queries = [
                Dx_CodeQuery(starts_with='784.0', type='icd9'),
                Dx_CodeQuery(starts_with='R51', type='icd10'),
                ]
            ))
        heuristic_list.append( DiagnosisHeuristic(
            name = 'myalgia',
            dx_code_queries = [
                Dx_CodeQuery(starts_with='729.1', type='icd9'),
                Dx_CodeQuery(starts_with='M79.1', type='icd10'),
                Dx_CodeQuery(starts_with='M79.10', type='icd10'),
                ]
            ))
        heuristic_list.append( DiagnosisHeuristic(
            name = 'anemia',
            dx_code_queries = [
                Dx_CodeQuery(starts_with='285.9', type='icd9'),
                Dx_CodeQuery(starts_with='D64.9', type='icd10'),
                ]
            ))
        heuristic_list.append( DiagnosisHeuristic(
            name = 'thrombocytopenia',
            dx_code_queries = [
                Dx_CodeQuery(starts_with='287.5', type='icd9'),
                Dx_CodeQuery(starts_with='D69.6', type='icd10'),
                ]
            ))
        heuristic_list.append( DiagnosisHeuristic(
            name = 'leukopenia',
            dx_code_queries = [
                Dx_CodeQuery(starts_with='288.5', type='icd9'),
                Dx_CodeQuery(starts_with='D72.819', type='icd10'),
                ]
            ))
        #
        # Prescriptions
        #
        for drug in [
            'doxycycline',
            'rifampin',
            ]:
         
            heuristic_list.append( PrescriptionHeuristic(
                name = drug,
                drugs = DrugSynonym.generics_plus_synonyms([drug]),
                ))
        #
        # Lab Results
        #
        heuristic_list.append( LabResultPositiveHeuristic(
            test_name = 'anaplasmosis_pcr',
            ))
        heuristic_list.append( LabResultPositiveHeuristic(
            test_name = 'anaplasmosis_hga_igg_80',
            titer_dilution=80,
            ))
        heuristic_list.append( LabResultPositiveHeuristic(
            test_name = 'anaplasmosis_hge_igg_64',
            titer_dilution=64,
            ))
        heuristic_list.append( LabResultPositiveHeuristic(
            test_name = 'anaplasmosis_hge_igm_20',
            titer_dilution=20,
            ))
        heuristic_list.append( LabResultPositiveHeuristic(
            test_name = 'anaplasmosis_hga_igm_16',
            titer_dilution=16,
            ))

        return heuristic_list


    def generate(self):
        LabTestMap.create_update_dummy_lab(self.conditions[0], 'MDPH-318', 'MDPH-R489')
        log.info('Generating cases of %s' % self.short_name)
        #
        # Criteria #1
        # Positive PCR lab test results
        #
        lx_pcr_event_names = [
                'lx:anaplasmosis_pcr:positive',
                ]

        lx_pcr_event_qs = Event.objects.filter(
                name__in = lx_pcr_event_names,
                )
        #
        # Criteria #2,3
        # Positive IFA (igg or igm) lab test result and (symptom dx_code or antibiotic)  
        # within 14 days of the IFA order date
        #
        rx_event_names = [
                'rx:doxycycline',
                'rx:rifampin',
                ]
        sx_event_names = [
                'dx:fever_unspecified',
                'dx:chills',
                'dx:headache',
                'dx:myalgia',
                'dx:anemia',
                'dx:thrombocytopenia',
                'dx:leukopenia',
                ]
        sx_rx_event_names = sx_event_names + rx_event_names
        lx_ifa_event_names = [
                'lx:anaplasmosis_hga_igg_80:positive',
                'lx:anaplasmosis_hge_igg_64:positive',
                'lx:anaplasmosis_hge_igm_20:positive',
                'lx:anaplasmosis_hga_igm_16:positive',
                ]
        lx_sx_rx_event_qs = Event.objects.filter(
                name__in = lx_ifa_event_names,
                patient__event__name__in = sx_rx_event_names,
                patient__event__date__gte = (F('date') - datetime.timedelta(days=14)),
                patient__event__date__lte = (F('date') + datetime.timedelta(days=14)),
                )
        #
        # Criteria #4
        # Diagnosis for Anaplasmosis and antibiotic within 14 days
        #
        dx_event_names = ['dx:anaplasmosis']
        dx_rx_event_qs = Event.objects.filter(
            name__in = dx_event_names,
            patient__event__name__in = rx_event_names,
            patient__event__date__gte = (F('date') - datetime.timedelta(days=14)),
            patient__event__date__lte = (F('date') + datetime.timedelta(days=14)),
            )

        #
        # Combined Criteria
        #
        combined_criteria_qs = (lx_pcr_event_qs | lx_sx_rx_event_qs | dx_rx_event_qs)
        combined_criteria_qs = combined_criteria_qs.exclude(
                                                    case__condition=self.conditions[0], 
                                                    ).order_by('date')
        counter = 0
        # create cases
        for this_event in combined_criteria_qs:
            case_event_qs = None
            # case criteria
            new__criteria = ''
            if this_event.name in lx_pcr_event_names:
                new_criteria = 'Criteria #1: positive A. phagocytophilum (PCR)'
            elif this_event.name in lx_ifa_event_names:
                new_criteria = 'Criteria #2/3: Positive IFA lab (igg or igm) and (symptom diagnosis code or antibiotic) within 14 days of the IFA'
                case_event_names = sx_rx_event_names
                case_event_qs = Event.objects.filter(
                        patient=this_event.patient,
                        name__in = case_event_names,
                        date__gte = this_event.date - datetime.timedelta(days=14),
                        date__lte = this_event.date + datetime.timedelta(days=14),
                        )
            elif this_event.name in dx_event_names:
                new_criteria = 'Criteria #4: anaplasmosis diagnosis code and a prescription for an antibiotic within 14 days'
                case_event_names = rx_event_names
                case_event_qs = Event.objects.filter(
                        patient=this_event.patient,
                        name__in = case_event_names,
                        date__gte = this_event.date - datetime.timedelta(days=14),
                        date__lte = this_event.date + datetime.timedelta(days=14),
                        )
            # create case
            if new_criteria:
                created, new_case = self._create_case_from_event_obj(
                    condition=self.conditions[0],
                    criteria=new_criteria,
                    recurrence_interval=self.recurrence,
                    event_obj=this_event,
                    relevant_event_qs=case_event_qs
                )
                if created:
                    log.debug('Created new anaplasmosis case: %s' % new_case)
                    counter += 1
        log.debug('Generating %s new cases of anaplasmosis' % counter)
        return counter # count of new cases

    def report_field(self, report_field, case):
        reportable_fields = {
            'na_trmt_obx': False,
            'symptom_obx': False,
            'NA-56': 'NA-1739',
            '10187-3': 'NA-1738',
        }

        return reportable_fields.get(report_field, None)
 
#-------------------------------------------------------------------------------
#
# Packaging
#
#-------------------------------------------------------------------------------


def event_heuristics():
    return Anaplasmosis().event_heuristics

def disease_definitions():
    return [Anaplasmosis()]

